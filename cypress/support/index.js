// ***********************************************************
// Default Cypress config
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// import './commands'
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
});
Cypress.on("before:browser:launch", (browser = {}, launchOptions) => {
    if (browser.family === "chromium") {
        launchOptions.args.push("--disable-dev-shm-usage");
    } 
    return launchOptions;
});
