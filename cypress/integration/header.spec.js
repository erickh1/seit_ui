const navList = require(`../fixtures/navList.json`);

context('Sample test on commbank.com.au', () => {
  beforeEach(() => {
    cy.visit('/');
  })

  describe('Check links', () => {
    for (let i=0; i< navList.headers.length; i++) {
      const navSet = navList.headers[i];
      it(`Check top nav: ${navSet['linkText']}`, () => {
        cy
          // Click on the top nav link
          .get(`.commbank-header .commbank-list a:contains('${navSet['linkText']}')`)
          .click()
    
          // Check page header
          .get(`.banner h1`)
          .should(`contain`, navSet['headerText'])
          
          // Also check footer to see that the whole page has loaded
          .get(`.footer h2:contains('About us')`)
          .should(`exist`)
      })
    }
  });
})
